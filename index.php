<?php
	function __autoload($classname) 
	{
		$class_type = explode('_', $classname);
		switch ($class_type[0]) {
		 	case 'C':
		 		require_once 'controllers/'.$classname.'.php';
		 		break;

		 	case 'M':
		 		require_once 'model/'.$classname.'.php';
		 		break;
		}
	}

	if(isset($_GET['c']))
	{
		switch ($_GET['c']) {

		case 'article':
			$controller = new C_Article();
			break;

		case 'editor':
			$controller = new C_Editor();
			break;

		case 'edit':
			$controller = new C_Edit();
			break;

		case 'new':
			$controller = new C_New();
			break;

		case 'login':
			$controller = new C_Login();
			break;

		default:
			$controller = new C_Index();
			break;
		}
	}
	else
		$controller = new C_Index();
	
	$controller->request();
?>