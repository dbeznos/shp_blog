<h2>Авторизация</h2>
<form role="form" method="post">
	<div class="form-group">
		<label for="login">Логин:</label>
		<input style="width: 300px;" class="form-control" id="login" type="text" name="login" />
	</div>
	<div class="form-group">
		<label for="password">Пароль:</label>
		<input style="width: 300px;" class="form-control" id="password" type="password" name="password" />
	</div>
	<div class="form-group">
		<label for="check">Запомить меня</label>
		<input id="check" type="checkbox" name="remember"/>
	</div>
	<input class="btn btn-default" type="submit" />		
</form>