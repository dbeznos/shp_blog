<!DOCTYPE html>
<html>
<head>
	<title>MVC Pattern Blog</title>
	<meta content="text/html; charset=utf-8" http-equiv="content-type">	
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
	<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" scr="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<h1>MVC Pattern Blog</h1>
		<br/>
		<a href="index.php">Главная</a> |
		<a href="index.php?c=editor">Консоль редактора</a> |
		<a href="index.php?c=login">Вход\Выход</a>
		<hr/>
			<?=$content?>
		<hr/>
		<?php
			if(isset($user) && !empty($user)) {
				echo $user['login'].'<br/><br/>';
			}
		?>
		<!-- <small><a href="http://prog-school.ru">Школа Программирования</a> &copy;</small> -->
	</div>
</body>
</html>