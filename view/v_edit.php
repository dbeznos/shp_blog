<h2>Редактирование статьи</h2>
<form role="form" method="post" path="index.php?c=edit">
	<div class="form-group">
		<label for="title">Название:</label>
		<input class="form-control" id="title" type="text" size="35" name="title" value="<?=$title?>" />
	</div>
	<div class="form-group">
		<label for="content">Содержание:</label>
		<textarea class="form-control" id="content" rows="10" name="content"><?=$content?></textarea>
	</div>
	<input class="btn btn-default" name="edit" type="submit" value="Редактировать" />
	<input class="btn btn-default" name="delete" type="submit" value="Удалить" />
</form>

<?php
//size="35" 
//cols="50" 
?>
