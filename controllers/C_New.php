<?php

    //
    // Контроллер страницы создания новой статьи
    //

	class C_New extends C_Base
    {
    	// для сохранения записей в полях в случае неудачного редактирования...
        private $title;
        private $text;

        // Виртуальный обработчик запроса.

        protected function OnInput()
        {
        	parent::OnInput();

            if($this->user == false)
                die('Оказано в доступе.');
            else{
                if(!$this->mUsers->can('VIEW_NEW'))
                    die('Оказано в доступе.');
            }
        	
            if (isset($_POST['title']))
			{
				if ($this->article_work->articles_new($_POST['title'], $_POST['content'], $this->link))
				{
					header('Location: index.php?c=editor');
					die();
				}
				
				$this->title = $_POST['title'];
				$this->content = $_POST['content'];
			}
			else
			{
				$this->title = '';
				$this->content = '';
			}    
        }

        // Виртуальный обработчик запроса.
        
        protected function OnOutput()
        {
            $this->content = $this->Template('view/v_new.php', array('content' => $this->content, 'title' => $this->title));
            parent::OnOutput();
        }
    }

?>




