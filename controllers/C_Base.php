<?php

	//
	// Базовый контроллер сайта 
	//

	class C_Base extends C_Controller
	{
		public $article_work;
		public $link;

		protected $user;
		protected $mUsers;

		// html для внутреннего шаблона
		protected $content;

		// Виртуальный обработчик запроса.

		protected function OnInput()
		{
			M_DBConnection::DBConnect();
			$this->article_work = M_Articles::Instance();
			$this->link = M_DBConnection::$link;

			$this->mUsers = M_Users::Instance();
            $this->mUsers->clearSessions();

            $this->user = $this->mUsers->get();
		}


		// Виртуальный обработчик запроса.

		protected function OnOutput()
		{
			$page = $this->Template('view/v_main.php', array('content' => $this->content, 'user' => $this->user));
			echo $page;
		}
	}
?>