<?php

	//
	//	Базовый класс контроллера
	//

	class C_Controller
	{
		function __constrict() {}


		// Полная обработка HTTP запроса.

		public function request()
		{
			$this->OnInput();
			$this->OnOutput();
		}

		// Виртуальный обработчик запроса.

		protected function OnInput() {}

		
		// Виртуальный генератор HTML.

		protected function OnOutput() {}


		// Запрос произведен методом GET?

		public function isGet()
		{
			return $_SERVER['REQUEST_METHOD'] == 'GET';
		}


		// Запрос произведен методом POST?

		public function isPost()
		{
			return $_SERVER['REQUEST_METHOD'] == 'POST';
		}


		// Генерация шаблона

		protected function Template($path, $vars = array())
		{
			// Установка переменных для шаблона.
			foreach ($vars as $key => $value) {
				$$key = $value;
			}

			// Генерация HTML в строку.
			ob_start();
			include $path;
			return ob_get_clean();
		}
	}
?>