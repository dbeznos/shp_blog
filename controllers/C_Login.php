<?php

    //
    // Контроллер страницы авторизации
    //

	class C_Login extends C_Base
    {
        protected function OnInput()
        {
        	parent::OnInput();
            
            $this->mUsers->logout();
            $this->user = null;
        	if($this->isPost())
            {
                if ($this->mUsers->login($_POST['login'], $_POST['password'], isset($_POST['remember']))) {
                    header("Location: index.php");
                    die();
                }
            }
        }

        // Виртуальный обработчик запроса.
        
        protected function OnOutput()
        {
            $this->content = $this->Template('view/v_login.php');
            parent::OnOutput();
        }
    }

?>
