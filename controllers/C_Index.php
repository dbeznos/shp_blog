<?php

    //
    // Контроллер главной страницы
    //

	class C_Index extends C_Base
    {
        private $articles;

        // Виртуальный обработчик запроса.

        protected function OnInput()
        {
            parent::OnInput();
            $this->articles = $this->article_work->articles_all($this->link);
            foreach ($this->articles as $key => $article)
                $this->articles[$key]['content'] = $this->article_work->articles_intro($article);         
        }

        // Виртуальный обработчик запроса.
        
        protected function OnOutput()
        {
            $this->content = $this->Template('view/v_index.php', array('articles' => $this->articles));
            parent::OnOutput();
        }
    }

?>




