<?php

    //
    // Контроллер страницы редактора
    //

	class C_Editor extends C_Base
    {
        private $articles;

        // Виртуальный обработчик запроса

        protected function OnInput()
        {
            parent::OnInput();
            $this->articles = $this->article_work->articles_all($this->link);
            
            if($this->user == false)
                die('Оказано в доступе.');
        }

        // Виртуальный обработчик запроса.
        
        protected function OnOutput()
        {
            $this->content = $this->Template('view/v_editor.php', array('articles' => $this->articles));
            parent::OnOutput();
        }
    }

?>




