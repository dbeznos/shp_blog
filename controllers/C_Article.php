<?php

    //
    // Контроллер страницы создания новой статьи
    //

	class C_Article extends C_Base
    {
        // Виртуальный обработчик запроса.

        private $article;

        protected function OnInput()
        {
        	parent::OnInput();
        	
            $this->article = $this->article_work->articles_get((int)$_GET['id'], $this->link);
        }

        // Виртуальный обработчик запроса.
        
        protected function OnOutput()
        {
            $this->content = $this->Template('view/v_article.php', array('article' => $this->article));
            parent::OnOutput();
        }
    }

?>




