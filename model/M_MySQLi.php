<?php
	class M_MySQLi
	{
		//выборка, тут всё ясно
		public function select($query, $link)
		{
			$result = mysqli_query($link, $query) or die(mysqli_error($link));

			$arr = array();
			while($row = mysqli_fetch_array($result))
				$arr[] = $row;

			return $arr;
		}

		//
		// Вставка строки
		// $table 		- имя таблицы
		// $object 		- ассоциативный массив с парами вида "имя столбца - значение"
		// результат	- идентификатор новой строки
		//

		public function insert($table, $object, $link)
		{
			$columns = array();
			$values = array();

			foreach ($object as $key => $value) {
				$key = mysqli_real_escape_string($link, $key.'');
				$columns[] = $key;

				if($value === NULL) {
					$values[] = 'NULL';
				}
				else {
					$value = mysqli_real_escape_string($link, $value.'');
					$values[] = "'$value'";
				}
			}

			$columns_s = implode(',', $columns);
			$values_s = implode(',', $values);

			$query = "INSERT INTO $table ($columns_s) VALUES ($values_s)";
			$result = mysqli_query($link, $query) or die(mysqli_error($link));

			return mysqli_insert_id($link);
		}

		//
		// Изменение строк
		// $table 		- имя таблицы
		// $object 		- ассоциативный массив с парами вида "имя столбца - значение"
		// $where		- условие (часть SQL запроса)
		// результат	- число измененных строк
		//	

		public function Update($table, $object, $where, $link)
		{
			$sets = array();
		
			foreach ($object as $key => $value)
			{
				$key = mysqli_real_escape_string($link, $key . '');
				
				if ($value === null) {
					$sets[] = "$key=NULL";			
				}
				else
				{
					$value = mysqli_real_escape_string($link, $value . '');					
					$sets[] = "$key='$value'";			
				}			
			}
			
			$sets_s = implode(',', $sets);			
			$query = "UPDATE $table SET $sets_s WHERE $where";
			$result = mysqli_query($link, $query) or die(mysqli_error($link));

			return mysqli_affected_rows($link);	
		}
		
		//
		// Удаление строк
		// $table 		- имя таблицы
		// $where		- условие (часть SQL запроса)	
		// результат	- число удаленных строк
		//		

		public function Delete($table, $where, $link)
		{
			$query = "DELETE FROM $table WHERE $where";		
			$result = mysqli_query($link, $query) or die(mysqli_error($link));

			return mysqli_affected_rows($link);	
		}
	}	
?>