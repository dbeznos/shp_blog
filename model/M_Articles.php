<?php

class M_Articles {

	private static $instance;

	private $sqlObj;

	public static function Instance()
	{
		if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
	}

	private function __construct(){
		$this->sqlObj = new M_MySQLi();
	}

	//
	// Список всех статей
	//
	function articles_all($link)
	{
		// Запрос.
		$query = "SELECT * FROM articles ORDER BY id_article DESC";

		$articles = $this->sqlObj->select($query, $link);

		return $articles;
	}

	//
	// Конкретная статья
	//
	function articles_get($id_article, $link)
	{
		$query = "SELECT * FROM articles WHERE id_article = '%d'";

		$query = sprintf($query, $id_article);

		$article = $this->sqlObj->select($query, $link);
		return $article[0];
	}

	//
	// Добавить статью
	//
	function articles_new($title, $content, $link)
	{
		// Подготовка.
		$title = trim($title);
		$content = trim($content);

		// Проверка.
		if ($title == '')
			return false;

		$object = array('title' => $title, 'content' => $content);

		$result = $this->sqlObj->insert('articles', $object, $link);
								
		if (!$result)
			return false;
			
		return true;
	}

	//
	// Изменить статью
	//
	function articles_edit($id_article, $title, $content, $link)
	{
		// Подготовка.
		$title = trim($title);
		$content = trim($content);

		// Проверка.
		if ($title == '')
			return false;

		$object = array('title' => $title, 'content' => $content);
		$where = "id_article = '$id_article'";

		$result = $this->sqlObj->update('articles', $object, $where, $link);

		if (!$result)
			return false;

		return true;
	}

	//
	// Удалить статью
	//
	function articles_delete($id_article, $link)
	{
		if (empty($id_article))
			return false;

		$where = "id_article = '$id_article'";

		$result = $this->sqlObj->delete('articles', $where, $link);

		if (!$result)
			return false;

		return true;
	}

	//
	// Короткое описание статьи
	//
	function articles_intro($article)
	{
		if(strlen($article['content']) > 100)
		 	return substr_replace(substr($article['content'], 0, 100), '...', -5);
		else
			return $article['content'];
	}
}

?>
