<?php
//Менеджер пользователей

class M_Users
{
	private static $instance;
	private $sqlObj;
	private $sid;	//идентификатор текущей сессии
	private $uid;	//идентификатор текуего пользователя
	private $onlineMap; //карта онлайн пользователей 

	public static function Instance()
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __construct()
    {
    	$this->sqlObj = new M_MySQLi();
    	$this->sid = null;
    	$this->uid = null;
    }

    public function clearSessions()
    {
    	$min = date('Y-m-d H:s:i', time() - 60 * 20);
    	$t = "time_last < '%s'";
    	$where = sprintf($t, $min);
    	$this->sqlObj->delete('sessions', $where, M_DBConnection::$link);
    }

    // Aфторизация
    // $remember - нужно ли запомнить в куках

    public function login($login, $password, $remember = true)
    {
    	// вытаскиваем полбзователя из БД
    	$user = $this->getByLogin($login);

    	if($user == null)
    		return false;

    	$id_user = $user['id_user'];

    	if($user['password'] != md5($password))
    		return false;

    	//запоминаем имя и md5 пароль
    	if($remember) {
    		$expire = time() + 3600 * 24 * 100;
    		setcookie('login', $login, $expire);
    		setcookie('password', md5($password), $expire);
    	}

    	$this->sid = $this->openSession($id_user);

    	return true;
    }

    // Выход
    public function logout()
    {
    	setcookie('login', '', time() - 1);
		setcookie('password', '', time() - 1);
		unset($_COOKIE['login']);
		unset($_COOKIE['password']);
		unset($_SESSION['sid']);		
		$this->sid = null;
		$this->uid = null;
    }

    // Получение пользователя
	// результат	- объект пользователя
    public function get($id_user = null)
    {
    	//если id_user не указан, берём его по текущей сессии
    	if($id_user == null)
    		$id_user = $this->getUId();

    	if($id_user == null)
    		return false;

    	// А теперь просто возвращаем пользователя по id_user.
		$t = "SELECT * FROM users WHERE id_user = '%d'";
		$query = sprintf($t, $id_user);
		$result = $this->sqlObj->select($query, M_DBConnection::$link);
		return $result[0];
    }

    public function getUId()
    {
    	//проверка кеша
    	if($this->uid != null)
    		return $this->uid;

    	$sid = $this->getSId();

    	if($sid == null)
    		return null;

    	$t = "SELECT id_user FROM sessions WHERE sid = '%s'";
    	$query = sprintf($t, mysqli_real_escape_string(M_DBConnection::$link, $sid));
		$result = $this->sqlObj->select($query, M_DBConnection::$link);

		// Если сессию не нашли - значит пользователь не авторизован.
		if (count($result) == 0)
			return null;
			
		// Если нашли - запоминм ее.
		$this->uid = $result[0]['id_user'];

		return $this->uid;
    }

    public function getSId()
    {
    	// Проверка кеша.
		if ($this->sid != null)
			return $this->sid;

		if(isset($_SESSION['sid']))
			$sid = $_SESSION['sid'];
		else
			$sid = null;

		//если нашли пробуем обновить timelast в базе
		//заодно проверим есть ли сессия там
		if($sid != null) {
			$session = array();
			$session['time_last'] = date('Y-m-d H:i:s');
			$t = "sid = '%s'";
			$where = sprintf($t, mysqli_real_escape_string(M_DBConnection::$link, $sid));
			$affected_rows = $this->sqlObj->Update('sessions', $session, $where, M_DBConnection::$link);

			if($affected_rows == 0) {
				$t = "SELECT count(*) FROM sessions WHERE sid = '%s'";
				$query = sprintf($t, mysqli_real_escape_string(M_DBConnection::$link, $sid));
				$result = $this->sqlObj->select($query, M_DBConnection::$link);

				if($result[0]['count(*)'] == 0)
					$sid = null;
			}
		}

		// Нет сессии? Ищем логин и md5(пароль) в куках.
		// Т.е. пробуем переподключиться.
		if($sid == null && isset($_COOKIE['login'])) {
			$user = $this->getByLogin($_COOKIE['login']);

			if($user != null && $user['password'] == $_COOKIE['password'])
				$sid = $this->openSession($user['id_user']);
		}

		if($sid != null)
			$this->sid = $sid;

		return $sid;
    }

    //
	// Проверка наличия привилегии
	// $priv 		- имя привилегии
	// $id_user		- если не указан, значит, для текущего
	// результат	- true или false
	//
	public function can($priv, $id_user = null)
	{		
		/*$t = "SELECT id_priv FROM privs WHERE name = '%s'";
		$query = sprintf($t, mysqli_real_escape_string(M_DBConnection::$link, $priv));
		$result = $this->sqlObj->select($query, M_DBConnection::$link);
		$id_priv = (int)$result[0][0];

		$query = "SELECT id_user FROM priv2roles INNER JOIN users USING(id_role) WHERE id_priv = '$id_priv'";
		$result = $this->sqlObj->select($query, M_DBConnection::$link);

		$can_users = array();
		foreach ($result as $key => $value)
			$can_users[] = $result[$key][0];

		$user = $this->get();

		foreach ($can_users as $key => $value) {
			if($can_users[$key] == $user['id_user'])
				return true;	
		}

		return false;*/

		$user = $this->get();

		if($id_user == null)
			$id_user = $this->getUId();

		if($id_user == null)
			return false;

		$t = "SELECT count(*) FROM users INNER JOIN priv2roles USING(id_role)
			  INNER JOIN privs USING(id_priv) WHERE privs.name = '%s' AND users.id_user = '%s'";
		$query = sprintf($t, mysqli_real_escape_string(M_DBConnection::$link, $priv), $id_user);
		$result = $this->sqlObj->select($query, M_DBConnection::$link);

		if($result[0][0] == 0)
			return false;
		else
			return true;
	}

	//
	// Проверка активности пользователя
	// $id_user		- идентификатор
	// результат	- true если online
	//
	public function isOnline($id_user)
	{		
		if($this->onlineMap == null) 
		{
			$query = "SELECT DISTINCT id_user FROM sessions";
			$result = $this->sqlObj->select($query, M_DBConnection::$link);

			$keys = array();
			foreach ($result as $key => $value) {
				$this->onlineMap[$key['id_user']] = true;
				$keys[] = $key;
			}
		}

		return @($this->onlineMap[$keys[$id_user]] != null);
	}


    // Получает пользователя из БД по логину
    public function getByLogin($login)
    {
    	$t = "SELECT * FROM users WHERE login = '%s'";
    	$query = sprintf($t, mysqli_real_escape_string(M_DBConnection::$link, $login));
    	$result = $this->sqlObj->select($query, M_DBConnection::$link);
    	return $result[0];
    }


    // Открытие новой сессии
    public function openSession($id_user)
    {
    	//генерируем SID
    	$sid = $this->generateStr(10);

    	$now = date('Y-m-d H:i:s');
    	//вставляем SID в БД
    	$session = array();
    	$session['id_user'] = $id_user;
    	$session['sid'] = $sid;
    	$session['time_start'] = $now;
    	$session['time_last'] = $now;
    	$this->sqlObj->insert('sessions', $session, M_DBConnection::$link);

    	$_SESSION['sid'] = $sid;

    	return $sid;
    }


    // Генерация случайной последовательности
    public function generateStr($lenght = 10)
    {
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
    	$code = "";
    	$clen = strlen($chars) - 1;

    	while (strlen($code) < $lenght)
    		$code .= $chars[mt_rand(0, $clen)];

    	return $code;
    }
}

?>