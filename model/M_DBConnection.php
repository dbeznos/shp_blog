<?php
class M_DBConnection
{
    const DB_HOST = 'localhost';
    const DB_LOGIN = 'root';
    const DB_PASSWORD = 'beznos22';
    const DB_NAME = 'shp_blog';

    public static $link;
    public static $instance;

    public static function Instance()
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct(){}

    public static function DBConnect()
    {
        self::$link = mysqli_connect(self::DB_HOST, self::DB_LOGIN, self::DB_PASSWORD, self::DB_NAME) or die('No connect with data base');

        /* change character set to utf8 */
        mysqli_set_charset(self::$link, 'utf8');
        header('Content-type: text/html; charset=utf-8');

        session_start();
    }
}
?>